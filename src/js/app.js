//该文件是webpack的入口文件，不仅仅用于汇总所有的js文件，该文件里还可以引入：json文件、less文件等等。
//引入模块
import '@babel/polyfill' // ES6新语法转换，但是会转换所有的(包括未使用的)
import '../css/index.less'