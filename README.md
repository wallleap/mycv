1. `npm install webpack webpack-cli -g`
3. `npm i`
4. `npm start`   /   `npm run build`





- webpack`npm install webpack -D`     https://webpackjs.com

- less`npm install -D  css-loader style-loader less-loader less`
- 语法检查`npm install eslint-loader eslint -D` https://eslint.org
- js语法转换`npm install -D babel-loader @babel/core @babel/preset-env`
- js兼容性处理`npm install @babel/polyfill` 按需引入 `npm install core-js`
- 样式中的图片`npm install file-loader url-loader --save-dev` imgs目录
- 打包HTML文件`npm install html-webpack-plugin -D` html中不能引入任何js、css
- HTML中图片`npm install html-loader -D`
- 其他资源 file-loader
- 自动编译打包运行`npm install webpack-dev-server -D`
- HMR热模替换
- 双环境配置文件
- 自动清除打包文件目录`npm install clean-webpack-plugin -D`
- 提取css成单独文件`npm install mini-css-extract-plugin -D`
- css文件兼容性处理`npm install postcss-loader postcss-fixbugs-fixes postcss-preset-env postcss-normalize autoprefixer -D`
- 压缩css`npm install optimize-css-assets-webpack-plugin -D`
- 压缩HTML html-webpack-plugin

